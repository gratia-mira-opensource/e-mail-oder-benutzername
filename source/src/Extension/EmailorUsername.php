<?php
/**
 * @package         Joomla.Plugin
 * @subpackage      Authentication.joomla
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license         GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Joomla\Plugin\Authentication\EmailorUsername\Extension;

defined('_JEXEC') or die;

use Joomla\CMS\Authentication\Authentication;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\User\User;
use Joomla\CMS\User\UserHelper;
use Joomla\Database\DatabaseAwareTrait;
use Joomla\CMS\Factory;


/**
 * Joomla Authentication plugin
 *
 * @since  1.5
 */
final class EmailorUsername extends CMSPlugin
{
	use DatabaseAwareTrait;

	/**
	 * This method should handle any authentication and report back to the subject
	 *
	 * @param   array    $credentials  Array holding the user credentials
	 * @param   array    $options      Array of extra options
	 * @param   object  &$response     Authentication response object
	 *
	 * @return  void
	 * @since   1.5
	 */
	public function onUserAuthenticate($credentials, $options, &$response)
	{

		// Get a database object
		$db    = Factory::getDbo();
		$query = $db->getQuery(true);

		$query->select('id, username, password');
		$query->from('#__users');
		$query->where('email = ' . $db->Quote($credentials['username']));

		$db->setQuery($query);
		$result = $db->loadObject();
		// print_r($result);
		// exit;


		if ($result)
		{
			// why mess with re-creating authentication - just use the system.
			$response->username = $result->username;
			$response->status   = Authentication::STATUS_SUCCESS;
		}
		else
		{
			$response->status        = Authentication::STATUS_FAILURE;
			$response->error_message = 'Login ungültig :(';
		}
	}
}
